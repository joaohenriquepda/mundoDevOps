
# Iniciando o projeto

Como dependência para esse projeto vamos precisar do Docker e Docker Compose instalados, caso esteja usando um sistema operacional baseado em Debian, poderá usar os scripts de instalação disponibilidados.
Para executar os scripts siga os comandos a seguir:

```
./installDocker.sh

./install_lattest_compose.sh

```

Inicialmente vamos precisar criar Dockerfile, e um arquivo para
dependências do Python e outro arquivo chamado docker-compose.yml.


1 - Vamos começar criando um diretório para o projeto, nesse exemplo
vamos criar um diretório chamado 'mundoDevOps'

```
mkdir mundoDevOps
```

2 - Dentro desse diretório vamos criar o arquivo Dockerfile.
O Dockerfile define o conteúdo da imagem de um aplicativo por meio de um ou mais comandos de compilação que configuram essa imagem. Uma vez construída, você pode executar a imagem em um contêiner.

```
cd mundoDevOps
touch Dockerfile
```
É adicionado ao Dockerfile as seguintes instruções:
```
FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/

```

3 - Agora vamos criar o arquivo para dependências do python. Esse arquivo é chamado de requirements.txt, ele é usado na criação da imagem
para instalar as dependências do python no momento que o contêiner é criado. Vamos adicionar nesse arquivo as seguintes dependências.

```
Django>=1.8,<2.0
psycopg2
```


4 - Agora vamos criar o arquivo docker-compose.yml, esse arquivo descreve qual serviços fazem parte do seu projeto. Para esse projeto temos o serviço web e o banco de dados. Esse arquivo ainda contém informações de como os serviços vão se ligar e quais portas estão disponiveis.


```
version: '3'

services:
  db:
    image: postgres
  web:
    build: .
    command: python3 manage.py runserver 0.0.0.0:8000
    volumes:
      - .:/code
    ports:
      - "8000:8000"
    depends_on:
      - db
```

5 - Em posse desses arquivos já podemos criar nosso projeto usando o contêiner Django, para isso vamos usar o seguinte comando.

```
docker-compose run web django-admin.py startproject mundoDevOps .

```

Após o contêiner ser criado será criado o projeto denominado mundoDevOps

### Conectando com banco

Como foi definido anteriormente, no arquivo docker-compose.yml temos o serviço de banco de dados que sobre um container do banco postgres. Agora vamos estabalecer a comunicação de acesso do projeto Django ao banco de dados. Para isso precisamos inserir a configuração dentro do arquivo seetings.py dentro do diretório do projeto. No arquivos settings vamos procurar e alterar o objeto identificado como DATABASES, as informações que devem ser inseridas são as descritas abaixo.

```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': 'db',
        'PORT': 5432,
    }
}
```

### Subindo todos os serviços

Após essas etapas de configuração e criação de containers, podemos agora subir todos os serviços pois a comunicação já está estabelecida.
O comando que vamos usar para isso é simples

```
docker-compose up

```

Em caso de sucesso na execução do comando, já podemos acessar no browser utilizando **localhost:8000**


# Automatizando o Deploy

**Para esse exemplo estou levando em conta que a configuração de acesso ao VPS através do SSH já está feita**

Para realizar o deploy da aplicação vamos utilizat o Dokku, para isso precisamos de alguns arquivos de configuração, vamos
precisar adicionar alguns pacotes de dependência de no arquivo requirements.txt


```
dj_database_url
gunicorn
```


**Procfile**

Esse arquivo contém uma simples lista para executar cada worker de trabalho, nesse caso vamos executar o uwsgi que irá executar o gunicorn. Vamos adicionar no arquivo Procfile


```
web: gunicorn core.wsgi — log-file -

```

Agora vamos informar para o Dokku algumas dependências  do projeto, essa informação deve está no runtime.txt

```
python-3.3.3

```

Agora vamos alterar as configurações do arquivo app.json adicionando o comando abaixo. O arquivo app.json diz ao Dokku o que ele deve executar
antes do deploy, nesse caso vamos executar o migrate do banco de dados. 


```
{
  "scripts": {
    "dokku": {
      "postdeploy": "./manage.py migrate"
    }
  }
}
```

#### Deploy Dokku

No seu VPS com Dokku instalado, vamos começar a criar nosso deploy


```
dokku apps:create mundoDevOps
dokku apps:list
```


Vamos instalar o plugin do postgres
```
dokku plugin:install https://github.com/dokku/dokku-postgres.git
```



Vamos criar o serviço do postgres
```
dokku postgres:create mundoDevOpsDB
```

Vamos linkar nosso serviço de banco de dados com nossa aplicação




### Conveviência
Podemos criar um script que irá executar os comandos do deploy no servidor Dokku. Criei o arquivo mkproject.sh e nele coloquei as instruções abaixo.


```
#!/bin/sh

randpw(){ < /dev/urandom tr -dc A-Za-z0-9 | head -c${1:-64};echo;}

if [ $# -ne 2 ];
    then echo "Usage:\n    mkproject <appname> <naked domain>"
    exit 1
fi

sudo dokku apps:create $1
sudo dokku postgres:create $1-db

sudo dokku postgres:link $1-db $1

sudo dokku config:set --no-restart $1 NODEBUG=1
sudo dokku config:set --no-restart $1 SECRET_KEY=`randpw`

read -p "Now push your code to Dokku, wait for it to deploy successfully and press any key here." mainmenuinput

sudo dokku domains:add $1 $2 www.$2
sudo dokku redirect:set $1 $2 www.$2

```

